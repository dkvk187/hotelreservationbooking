package com.mobi.hotelbooking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.mobi.hotelbooking.service.BookingDetailsService;

@Controller
public class MainController {

	@Autowired
	BookingDetailsService bookingDetailsService;

	@GetMapping("/register")
	public String showForm(Model model) {
		BookingDetails bookingDetails = new BookingDetails();
		model.addAttribute("bookingDetails", bookingDetails);
		
		return "register_form";
	}

	@GetMapping("/mobireservation")
	public String mobireservation(@ModelAttribute("user") User user,Model model) {
		System.out.println(user);

	
		return "mobireservation";
	}

	@PostMapping("/register")
	public String submitForm(@ModelAttribute("user") User user) {
		System.out.println(user);
		return "register_success";
	}

	@GetMapping("/checkout")
	public String checkout(Model model) {
		BookingDetails bookingDetails = new BookingDetails();
		model.addAttribute("bookingDetails", bookingDetails);
		return "checkout";
	}

	@PostMapping("/confirmation")
	public String bookRoom(@ModelAttribute("bookingDetails") BookingDetails bookingDetails) {
		// model.addAttribute("bookingDetails", bookingDetails);
		return "register_success";
	}

	@PostMapping("/checkandupdate")
	public String confirmBooking(@ModelAttribute("bookingDetails") BookingDetails bookingDetails) {
		int result = bookingDetailsService.insertBookingDetails(bookingDetails);
		if (result == 99) {
			bookingDetails.setErrorMessage(
					"for same mobile number more than 10 rooms are booked on same check-In, Check-out date");

		} else if (result == 999) {
			bookingDetails.setErrorMessage(
					"no rooms available on the check-In, check-Out date provided. Please choose another date. Thanks.!!");
		} else if (result >= 1) {
			// fetch the registration number for the booking
			int registrationNumber = bookingDetailsService.fetchResitrationNumber(bookingDetails);
			bookingDetails.setRegNo(String.valueOf(registrationNumber));
			return "checkandupdate";
		}
		return "bookingError";

	}
	
	@PostMapping("/checkoutconfirmation")
	public String checkOutConfirmation(@ModelAttribute("bookingDetails") BookingDetails bookingDetails) {
		// model.addAttribute("bookingDetails", bookingDetails);
		return "checkout_success";
	}
}
