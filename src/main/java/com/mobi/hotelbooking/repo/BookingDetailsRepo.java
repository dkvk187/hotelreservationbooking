package com.mobi.hotelbooking.repo;

import com.mobi.hotelbooking.BookingDetails;

public interface BookingDetailsRepo {

	public int insertBookingDetails(BookingDetails bookingDetails);

	public int updateBookingDetails(BookingDetails bookingDetails);

	public int fetchBookingDetails(BookingDetails bookingDetails);

	public int fetchRoomsCount(BookingDetails bookingDetails);

	public int fetchRoomsCountOnMob(BookingDetails bookingDetails);

	public int fetchRegistrationNumber(BookingDetails bookingDetails);
}
