package com.mobi.hotelbooking.repo;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.mobi.hotelbooking.BookingDetails;

@Repository
public class BookingDetailsRepoImpl implements BookingDetailsRepo {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insertBookingDetails(BookingDetails bookingDetails) {
		String sql = "INSERT INTO mobiHotelBooking.BOOKING_DETAILS(mobileNumber,bookingName,checkInDate,checkOutdate,numberOfRooms) VALUES(?,?,?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int result = 0;
		try {

			result = jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, bookingDetails.getMobileNumber());
				ps.setString(2, bookingDetails.getName());
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm");
				Date parsedCheckInDate = null;
				try {
					parsedCheckInDate = dateFormat.parse(bookingDetails.getCheckInDate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Timestamp parsedCheckInDateTime = new Timestamp(parsedCheckInDate.getTime());
				System.out.println(parsedCheckInDateTime.toLocalDateTime());

				ps.setTimestamp(3, Timestamp.valueOf(parsedCheckInDateTime.toLocalDateTime()));

				Date parsedCheckOutDate = null;
				try {
					parsedCheckOutDate = dateFormat.parse(bookingDetails.getCheckInDate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Timestamp parsedCheckOutDateTime = new Timestamp(parsedCheckOutDate.getTime());
				ps.setTimestamp(4, Timestamp.valueOf(parsedCheckOutDateTime.toLocalDateTime()));
				ps.setString(5, bookingDetails.getNumberOfRooms());
				return ps;
			}, keyHolder);

		} catch (DuplicateKeyException exception) {
			exception.printStackTrace();
			result = -1;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("excpetion " + e);

		}
		return result;
	}

	@Override
	public int updateBookingDetails(BookingDetails bookingDetails) {

		return 0;
	}

	@Override
	public int fetchBookingDetails(BookingDetails bookingDetails) {
		int result = 0;
		try {
		String sql = "SELECT COUNT(*) FROM mobiHotelBooking.BOOKING_DETAILS WHERE checkInDate = ?";

		result = jdbcTemplate.queryForObject(sql, new Object[] { bookingDetails.getCheckInDate() }, Integer.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm");
		Date parsedDate = dateFormat.parse("2020-12-15T11:37");
		Timestamp parsed = new Timestamp(parsedDate.getTime());
		System.out.println(parsed.toLocalDateTime());
	}

	@Override
	public int fetchRoomsCount(BookingDetails bookingDetails) {
		int roomsCount = 0;
		try {
		String sql = "SELECT \r\n" + 
				"    sum(numberOfRooms)\r\n" + 
				"FROM\r\n" + 
				"    (SELECT \r\n" + 
				"        *\r\n" + 
				"    FROM\r\n" + 
				"        mobihotelbooking.booking_details\r\n" + 
				"    WHERE\r\n" + 
				"        DATE(checkInDate) = DATE(?)) AS T\r\n" + 
				"WHERE\r\n" + 
				"    DATE(checkOutDate) = DATE(?)";
		roomsCount = jdbcTemplate.queryForObject(sql,
				new Object[] { bookingDetails.getCheckInDate(), bookingDetails.getCheckOutDate() }, Integer.class);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return roomsCount;
	}

	@Override
	public int fetchRoomsCountOnMob(BookingDetails bookingDetails) {
		int roomsCount = 0;
		try {
		String sql = "SELECT \r\n" + 
				"    sum(numberOfRooms)\r\n" + 
				"FROM\r\n" + 
				"    (SELECT \r\n" + 
				"        *\r\n" + 
				"    FROM\r\n" + 
				"        mobihotelbooking.booking_details\r\n" + 
				"    WHERE\r\n" + 
				"        DATE(checkInDate) = DATE(?) AND mobileNumber=?) AS T\r\n" + 
				"WHERE\r\n" + 
				"    DATE(checkOutDate) = DATE(?) AND mobileNumber=?";
		roomsCount = jdbcTemplate.queryForObject(sql,
				new Object[] { bookingDetails.getCheckInDate(),bookingDetails.getMobileNumber(), bookingDetails.getCheckOutDate(),bookingDetails.getMobileNumber() }, Integer.class);
		} catch(NullPointerException exception) {
			System.out.println("No data available");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return roomsCount;
	}

	@Override
	public int fetchRegistrationNumber(BookingDetails bookingDetails) {
		int registrationNumber = 0;
		try {
		String sql = "SELECT \r\n" + 
				"    registrationNumber\r\n" + 
				"FROM\r\n" + 
				"    mobihotelbooking.booking_details\r\n" + 
				"WHERE\r\n" + 
				"    DATE(checkInDate) = DATE(?)\r\n" + 
				"        AND DATE(checkOutDate) = DATE(?)\r\n" + 
				"        AND mobileNumber = ?\r\n" + 
				"        AND bookingName = ?\r\n" + 
				"        AND numberOfRooms = ? AND bookingTime IN (SELECT \r\n" + 
				"            MAX(bookingTime)\r\n" + 
				"        FROM\r\n" + 
				"            mobihotelbooking.booking_details)";
		
		registrationNumber = jdbcTemplate.queryForObject(sql,
				new Object[] { bookingDetails.getCheckInDate(),bookingDetails.getCheckOutDate(),bookingDetails.getMobileNumber(), bookingDetails.getName(),bookingDetails.getNumberOfRooms() }, Integer.class);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return registrationNumber;
	}

}
