package com.mobi.hotelbooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobi.hotelbooking.BookingDetails;
import com.mobi.hotelbooking.repo.BookingDetailsRepo;

@Service
public class BookingDetailsService {

	@Autowired
	BookingDetailsRepo bookingDetailsRepo;

	public int insertBookingDetails(BookingDetails bookingDetails) {
		int result = 0;

		// get check-in, check-out date and check number of rooms available
		int roomsBooked = bookingDetailsRepo.fetchRoomsCount(bookingDetails);
		System.out.println("---roomsBooked-----" + roomsBooked);
		if (roomsBooked < 10 && (roomsBooked + Integer.parseInt(bookingDetails.getNumberOfRooms())) <= 10) {
			// get mobile number and check for the same date any room booked?
			int roomsBookedOnMob = bookingDetailsRepo.fetchRoomsCountOnMob(bookingDetails);
			if (roomsBookedOnMob < 10
					&& (roomsBookedOnMob + Integer.parseInt(bookingDetails.getNumberOfRooms())) <= 10) {
				result = bookingDetailsRepo.insertBookingDetails(bookingDetails);
			} else {
				// for same mobile number more than 10 rooms are booked on same check-In,
				// Check-out date
				result = 99;
			}
		} else {
			// no rooms available on the check-In, check-Out date provided
			result = 999;
		}
		// result = bookingDetailsRepo.insertBookingDetails(bookingDetails);
		System.out.println("----insertBookingDetails Service----" + result);
		return result;
	}

	public int fetchBookingDetails(BookingDetails bookingDetails) {
		int result = 0;
		result = bookingDetailsRepo.fetchBookingDetails(bookingDetails);
		return result;

	}

	public int fetchResitrationNumber(BookingDetails bookingDetails) {
		int registrationNumber = 0;
		registrationNumber = bookingDetailsRepo.fetchRegistrationNumber(bookingDetails);
		return registrationNumber;
	}

}
